<?php

class TextExtractionStart
{
    public function text_extraction_start(SugarBean $bean, $event, $arguments)
    {
		require_once('include/SugarQueue/SugarJobQueue.php');
		
		//非同期処理に必要なパラメーターが無い場合は処理しない
		if(isset($bean->id, $bean->filename)){
			$job = new SchedulersJob();
			
			$this->chkJobQueue($job, $bean);
		}
		
	}
	
	//jobqチェック
	public function chkJobQueue($job, $bean){
		
		$order = "";
		$where = sprintf("job_queue.data = '%s'",$bean->id);
		$jobqueue_list = $job->get_full_list($order,$where);
		
		//jobq にデータが無ければ、未登録なのでjobq登録⇒テキスト抽出処理へ
		if(!$jobqueue_list){
			$job->name = "TextExtraction - {$bean->name} - Create";
			$job->data = $bean->id;
			$job->target = "class::TextExtraction";
			
			$jq = new SugarJobQueue();
			$jobid = $jq->submitJob($job);
		}else{
			//レコードが存在した場合
			$order = "";
			$where = sprintf("job_queue.data = '%s' and (job_queue.status = 'queued' or job_queue.status = 'running')",$bean->id);
			$jobqueue_list = $job->get_full_list($order,$where);
			
			if(!$jobqueue_list){
				$job->name = "TextExtraction - {$bean->name} - Update";
				$job->data = $bean->id;
				$job->target = "class::TextExtraction";
				
				$jq = new SugarJobQueue();
				$jobid = $jq->submitJob($job);
			}
		}
	}
	
}
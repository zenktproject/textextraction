<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class TextExtraction implements RunnableSchedulerJob{
	private $tika_app_ = "tika-app-1.5.jar";			//tika-app ファイル名
	private $option_ = " -t --encoding=utf-8 ";			//テキスト抽出コマンドオプション※半角スペース重要※
	private $cmd_ = "java -Xmx128m -jar ";				//コマンド前半部分※半角スペース重要※
	private $config_ = "export LANG=ja_JP.UTF-8; ";		//環境変数設定
	
	public function setJob(SchedulersJob $job) {
		$this->job = $job;
	}

	public function run($data){
		$bean = BeanFactory::getBean('Notes', $data);
		global $sugar_config;						//config情報
		$upload_dir = $sugar_config['upload_dir'];	//Sugarアップロードフォルダ
		$tika_path = $sugar_config['tika_path'];	//tika-app 格納フォルダ※config_override記述
		
		$full_path = realpath(".");					//絶対パス取得
		$file_path = $full_path."/".$upload_dir;	//Sugarファイルアップロードフォルダパス生成
		
		$note_id = $bean->id;						//レコードID
		$file_name = $note_id;						//アップロードファイル名 = レコードID
		$file = $file_path.$file_name;				//アップロードファイルを含む絶対パス

		//コマンド生成
		$command = $this->cmd_.$tika_path.$this->tika_app_.$this->option_.$file;
		
		//シェル実行
		$result = shell_exec($this->config_.$command);
		
		//改行コード除去
		$trim_result = str_replace(array("\r\n","\n","\r"),"",$result);
		
		//Note更新
		$bean->document_fulltext_c = $trim_result;
		$bean->save();
		return true;
	}
}